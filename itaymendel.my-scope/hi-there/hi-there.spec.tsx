import React from 'react';
import { render } from '@testing-library/react';
import { BasicHiThere } from './hi-there.composition';

describe('hi-there', () => {
  it('should render with the correct text', () => {
    const { getByText } = render(<BasicHiThere />);
    const rendered = getByText('hello from HiThere');
    expect(rendered).toBeTruthy();
  });
});
