import React from 'react';
import { HiThere } from './hi-there';

export const BasicHiThere = () => (
  <HiThere text="hello from HiThere" />
);
