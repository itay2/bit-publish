import React from 'react';

export type HiThereProps = {
  /**
   * a text to be rendered in the component.
   * 
   */
  text: string
};

export function HiThere({ text }: HiThereProps) {
  return (
    <div>
      {text}
    </div>
  );
}
